#!/bin/bash

#SBATCH --mem=200G
#SBATCH --partition=court
#SBATCH --job-name=job3
#SBATCH --output=job_%a.out
#SBATCH --nodelist=kephren
##SBATCH --exclusive
#SBATCH --array=75,48,33

cd ..

config=$((SLURM_ARRAY_TASK_ID))

# Exécution de la simulation
./replicationSimulation.sh 1 configToyEpidemicalContext ./generationConfigDepartement/configDpt$config

# Collecte des informations sur la RAM maximale utilisée
max_ram=$(sstat -j $SLURM_JOB_ID --format=MaxRSS | tail -n 1)

echo "La RAM maximale utilisée par le job est de$max_ram"